//insert the number of the first lane to keep Right Arrow 
//                        here
const int firstExitLane = 3;

RoadAI AI = (RoadAI)((NetInfo)(ToolsModifierControl.toolController.m_editPrefabInfo)).m_netAI;
NetInfo[] allElevations = { AI.m_bridgeInfo, AI.m_elevatedInfo, AI.m_info, AI.m_slopeInfo, AI.m_tunnelInfo };
foreach (NetInfo asset in allElevations)
{
    int x = firstExitLane;
    foreach (NetInfo.Lane l in asset.m_lanes)
    {
        if (l.m_laneType == NetInfo.LaneType.Vehicle && x > 1)
        {
            l.m_laneProps.m_props = new NetLaneProps.Prop[0];
            x--;
        }
        if (l.m_laneType == NetInfo.LaneType.Vehicle && x == 1)
        {
            foreach (NetLaneProps.Prop p in l.m_laneProps.m_props)
            {
                if (p.m_prop.name == "Road Arrow R")
                {
                    l.m_laneProps.m_props = new NetLaneProps.Prop[1];
                    l.m_laneProps.m_props[0] = p;
                    break;
                }
                else
                    l.m_laneProps.m_props = new NetLaneProps.Prop[0];
            }
        }
    }
}
Debug.Log("operation successful");